import json
import time
# import fcntl
# import os
import logging
import requests

logging.basicConfig(filename='aviowiki.log',level=logging.DEBUG,format='%(asctime)s %(message)s')
minimum_interval = 5  # Minimum seconds between data refresh

uuidFile = '/var/aviowiki/id'

# Retrieve box UUID
with open(uuidFile) as f:
    uuid = f.read()

last_refresh = 0

while True:

    try:
        with open('/var/run/dump1090-fa/aircraft.json', 'r') as f:
            # fd = f.fileno()
            # flag = fcntl.fcntl(fd, fcntl.F_GETFL)
            # fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
            # flag = fcntl.fcntl(fd, fcntl.F_GETFL)
            # if flag & os.O_NONBLOCK:
            positions = json.load(f)
            del f
            # else:
            #     logging.error('Error in opening input file. Trying again...')
            #     time.sleep(5)
            #     continue

    except Exception as e:
        logging.error('Error in opening input file. Trying again...')
        logging.error('Error was: ' + str(e))
        time.sleep(1)
        continue

    if positions['now'] < last_refresh + minimum_interval or 'now' not in positions:
        logging.debug('Data not yet updated. Trying again in 1 second...')
        time.sleep(1)
        continue
    else:
        last_refresh = positions['now']

    try:
        # Prepare the request for the server
        url = 'https://ioa.aviowiki.com/send'
        headers = {'aviowiki-box-id': uuid}
        payload = positions

        # Send the data to the server
        r = requests.post(url, headers=headers, data=json.dumps(payload))

    except Exception as e:
        logging.error('Something went wrong in sending data to the server. Will skip this round.')
        continue
