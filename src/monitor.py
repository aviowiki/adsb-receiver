import requests
import fcntl
import os
import json

uuidFile = '/var/aviowiki/id'
versionFile = '/opt/aviowiki/version'
statsFile = '/var/run/dump1090-fa/stats.json'

# Retrieve box UUID
with open(uuidFile) as f:
    uuid = f.read()

# Retrieve box software version
with open(versionFile) as f:
    version = f.read()

# Non-blocking opening of stats file
with open(statsFile, "r") as f:
    fd = f.fileno()
    flag = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
    flag = fcntl.fcntl(fd, fcntl.F_GETFL)
    if flag & os.O_NONBLOCK:
        stats = json.load(f)


# Get the CPU temperature
def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    temp = res.replace('temp=','').replace("'C\n","")
    return float(temp)


cpu_temp = getCPUtemperature()

# Prepare the request for the server
url = 'https://ioa.aviowiki.com/box/monitor'
headers = {'aviowiki-box-id': uuid}
payload = {'cpu_temp': cpu_temp,
           'last_15': stats['last15min'],
           'sw_version': version}

r = requests.post(url, headers=headers, data=json.dumps(payload))
