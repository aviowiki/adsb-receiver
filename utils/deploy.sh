#!/bin/bash

# Automated deployment of the aviowiki ADS-B receiver
# ---------------------------------------------------

# Set working directory	to where the script is located
cd "$(dirname "$0")"

# update the fetch script. This will only take effect on the next run!
cp ./adsb-receiver/utils/fetch.sh ./fetch.sh

# Copy the main code
cp ./adsb-receiver/src/core.py /opt/aviowiki/core.py

# empty the server's working folder
rm -rf /opt/aviowiki/*

# rebuild the virtual environment based	on the requirements file
/usr/local/bin/virtualenv --clear /opt/aviowiki/virtualenv
/opt/aviowiki/virtualenv/bin/pip install -r ./adsb-receiver/utils/requirements.txt

# move new source files	to server directory
cp -r ./adsb-receiver/src/. /opt/aviowiki/

# stop aviowiki service and replace the systemd configuration file
systemctl stop aviowiki
cp ./adsb-receiver/utils/aviowiki.service /etc/systemd/system/aviowiki.service
chmod 644 /etc/systemd/system/aviowiki.service
systemctl enable aviowiki
systemctl daemon-reload
systemctl start aviowiki

# Manage cron jobs
# ----------------

# Cron parameters for automated monitoring
croncmd="/opt/aviowiki/virtualenv/bin/python /opt/aviowiki/monitor.py"
cronjob="*/15 * * * * $croncmd"
# Add the job to the crontab, with no duplication:
( crontab -l | grep -v -F "$croncmd" ; echo "$cronjob" ) | crontab -

# Cron parameters for automated deployment
croncmd="sh /opt/deploy/fetch.sh"
cronjob="*/30 * * * * $croncmd"
# Add the job to the crontab, with no duplication:
( crontab -l | grep -v -F "$croncmd" ; echo "$cronjob" ) | crontab -

# Remove it from the crontab whatever its current schedule:
# ( crontab -l | grep -v -F "$croncmd" ) | crontab -

# Restart system
shutdown -r now