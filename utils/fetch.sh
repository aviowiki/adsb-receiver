#!/bin/bash

# Automated fetching of the code from Git
# ---------------------------------------

# Set working directory	to where the script is located
cd "$(dirname "$0")"
rm -rf adsb-receiver

# retrieve files from git
git clone https://bitbucket.org/aviowiki/adsb-receiver.git

# start deployment
cp ./adsb-receiver/utils/deploy.sh ./deploy.sh
sh ./deploy.sh